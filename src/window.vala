[GtkTemplate (ui = "/org/example/App/window.ui")]
public class Tabbutton.Window : Gtk.ApplicationWindow {
	[GtkChild]
	private TabButton btn1;
	[GtkChild]
	private TabButton btn2;
	[GtkChild]
	private TabButton btn3;
	[GtkChild]
	private TabButton btn4;
	[GtkChild]
	private Gtk.Adjustment adj;

	public Window (Gtk.Application app) {
		Object (application: app);
	}

	construct {


		adj.value_changed.connect(() => {
			int n_tabs = (int) adj.value;
			btn1.n_tabs = n_tabs;
			btn2.n_tabs = n_tabs;
			btn3.n_tabs = n_tabs;
			btn4.n_tabs = n_tabs;
		});
	}
}
