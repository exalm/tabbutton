[GtkTemplate (ui = "/org/example/App/tab-button.ui")]
public class Tabbutton.TabButton : Gtk.Button {
	// Copied from GtkInspector code
	private const double XFT_DPI_MULTIPLIER = 1.0 / 96.0 / 1024.0;

	[GtkChild]
	private Gtk.Label tabs_label;
	[GtkChild]
	private Gtk.Image tabs_icon;

	private int _n_tabs;
	public int n_tabs {
		get { return _n_tabs; }
		set {
			if (_n_tabs == value)
				return;

			_n_tabs = value;

			bool overflow = n_tabs >= 100;

			tabs_label.label = "%d".printf(n_tabs);
			tabs_label.visible = !overflow;
			tabs_icon.icon_name = overflow
				? "tab-overflow-symbolic"
				: "tab-counter-symbolic";
		}
	}

	construct {
		n_tabs = 8;
		xft_dpi_changed ();
		Gtk.Settings.get_default ().notify["gtk-xft-dpi"].connect (xft_dpi_changed);
	}

	// FIXME: I hope there is a better way to prevent label from changing scale
	private void xft_dpi_changed () {
		double xft_dpi = Gtk.Settings.get_default ().gtk_xft_dpi * XFT_DPI_MULTIPLIER;

		tabs_label.attributes.change (Pango.attr_scale_new (1 / xft_dpi));
	}
}
